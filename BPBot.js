const TelegramBot = require('node-telegram-bot-api');
const dbot = require('dbot-js');

const token = '949855974:AAFKuxyBXiATwPRwC9fjZiXVietzkKSE3mw'

const bot = new TelegramBot(token, {polling:true});

bot.onText(/\/echo (.+)/, (msg, match) => {
    // 'msg' is the received Message from Telegram
    // 'match' is the result of executing the regexp above on the text content
    // of the message
  
    const chatId = msg.chat.id;
    const resp = match[1]; // the captured "whatever"
  
    // send back the matched "whatever" to the chat
    bot.sendMessage(chatId, "This is chat message: " + resp);
  });
  
bot.onText(/\/whoIsThis/, (msg) => {
    // 'msg' is the received Message from Telegram
    // 'match' is the result of executing the regexp above on the text content
    // of the message
  
    const chatId = msg.chat.id;
  
    // send back the matched "whatever" to the chat
    bot.sendMessage(chatId, "My name is BPBot, i can help you to keep you update about what happen in the BPROA ");
  });
   
  // Listen for any kind of message. There are different kinds of
  // messages.
  // bot.on('message', (msg) => {
  //   const chatId = msg.chat.id;
  
  //   // send a message to the chat acknowledging receipt of their message
  //   bot.sendMessage(chatId, "This is chat ID: " + chatId);
  // });

  bot.onText(/\/checkId/, (msg, match) => {
  
    const chatId = msg.chat.id;
  
    // send back the matched "whatever" to the chat
    bot.sendMessage(chatId, "This is chat Id sender: " + chatId);
  });

bot.onText(/\/help/, (msg, match) => {
    // 'msg' is the received Message from Telegram
    // 'match' is the result of executing the regexp above on the text content
    // of the message
  
    const chatId = msg.chat.id;
    const resp = match[1]; // the captured "whatever"
  
    // send back the matched "whatever" to the chat
    bot.sendMessage(chatId, " /help - For displaying help \n/whoIsThis - Explanation about this bot \n/checkId - For check sender ID \n/echo [message] - For sending message");
  });
  