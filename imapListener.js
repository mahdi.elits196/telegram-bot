var MailListener = require("mail-listener2");
 
var mailListener = new MailListener({
  username: "mtm.roa@gmail.com",
  password: "mtmroa2019&",
  host: "imap.gmail.com",
  port: 993, // imap port
  tls: true,
  connTimeout: 10000, // Default by node-imap
  authTimeout: 5000, // Default by node-imap,
  debug: console.log, // Or your custom function with only one incoming argument. Default: null
  tlsOptions: { rejectUnauthorized: false },
  mailbox: "INBOX", // mailbox to monitor
//   searchFilter: ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
  searchFilter : ["UNSEEN"],
  markSeen: true, // all fetched email willbe marked as seen and not fetched next time
//   fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
//   mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
//   attachments: true, // download attachments as they are encountered to the project directory
//   attachmentOptions: { directory: "attachments/" } // specify a download directory for attachments
});
 
mailListener.start(); // start listening
 
// stop listening
//mailListener.stop();
 
mailListener.on("server:connected", function(){
  console.log("imapConnected");
});
 
// mailListener.on("server:disconnected", function(){
//   console.log("imapDisconnected");
// });
 
// mailListener.on("error", function(err){
//   console.log(err);
// });
 
mailListener.on("mail", function(mail, seqno, attributes){
  // do something with mail object including attachments
  console.log("Subject", mail.subject);
  console.log("Content", mail.text);
  // mail processing code goes here
});